STDM Laboratory work No.3
Topic: Behavioral Design Patterns
Prepared by : Latcovschi Alexandru , FAF-181
Evaluated by : Drumea Vasile

#Main tasks :
Objectives:
1. Study and understand the Behavioral Design Patterns;

2. As a continuation of the previous laboratory work, think about what communication between software entities might be involed in your system;

3. Implement some additional functionalities using behavioral design patterns;

## Theoretical background:
In software engineering, behavioral design patterns have the purpose of identifying common communication patterns between different software entities. By doing so, these patterns increase flexibility in carrying out this communication.

Some examples from this category of design patterns are :

Chain of Responsibility
Command
Interpreter
Iterator
Mediator
Observer
Strategy
## Main tasks :
1. By extending your project, implement as many behavioral design patterns as you need in your project:

The implemented design pattern should help to perform the tasks involved in your system;
The object creation mechanisms/patterns can now be buried into the functionalities instead of using them into the client;
The behavioral DPs can be integrated into you functionalities alongside the structural ones;
There should only be one client for the whole system;
2. Keep your files grouped (into packages/directories) by their responsibilities (an example project structure):

client;
domain;
utilities;
data(if applies);
3. Document your work in a separate markdown file according to the requirements presented below (the structure can be extended of course):

Topic of the laboratory work;
Author;
Introduction/Theory/Motivation;
Implementation & Explanation (you can include code snippets as well):
Indicate the location of the code snippet;
Emphasize the main idea and motivate the usage of the pattern;
Results/Screenshots/Conclusions;

#Theory:
Behavioral design patterns are concerned with the interaction and responsibility of objects.

In these design patterns,the interaction between the objects should be in such a way that they can easily talk to each other and still should be loosely coupled.

That means the implementation and the client should be loosely coupled in order to avoid hard coding and dependencies.

##Types of behavioral design patterns used in this lab
1. Iterator Pattern
+ Iterator Pattern is used "to access the elements of an aggregate object sequentially without exposing its underlying implementation".
The Iterator pattern is also known as Cursor.

2. State Pattern
A State Pattern says that "the class behavior changes based on its state". In State Pattern, we create objects which represent various states and a context object whose behavior varies as its state object changes.

3. Strategy Pattern
A Strategy Pattern says that "defines a family of functionality, encapsulate each one, and make them interchangeable".

# Implementation & Explanation
      In this project I implemented 3 Behavior Design Patterns:  
      + Iterator Pattern - we can itterate an array and logitems in the console
      + State Pattern - a collection of logs for changes in database schemas,
      for open, change, logs and close
      + Strategy Pattern - I use 2 oppeations with input data for users, first I get full name from separated
      first name and last name, second is slug generator from first name and last name.

## To run laboratory 3 just run runLaboratory3() from main!                                                

# Results:
FullName: Alexandru Latcovschi
FullName: Gradinaru Daniela
FullName: Elon Musk
FullName: Mihai Cebotari
FullName: Nedelea Damian



Open database for Management
Log activities
Close the database
Management has been updated



Enter the first name: Alexandru
Enter the last name: Latcovschi



Full Name = Alexandru Latcovschi
Slug From Name = alexandruLatcovschi

# Conclusion: 
In a project in which we need optimization tools and good structure, behavioral design patterns
are very useful and easy to use, they help us save more time and make 
everything more optimized. It is very important to use them correctly,
 so as not to do worse than it would have been without them. 
 There are many design patterns, but I chose the ones that help me the 
 most in working with the data I chose. It is very important to do everything 
 as dynamically as possible so that we can easily adapt and not do the same 
 thing twice.
