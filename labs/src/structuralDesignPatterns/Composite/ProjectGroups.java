package structuralDesignPatterns.Composite;
//package src;

import java.util.ArrayList;
import java.util.List;

public class ProjectGroups {
    private String name;
    private String type;
    private int donations;
    private List<ProjectGroups> team;

    public ProjectGroups(String name, int donations, String type) {
        this.name = name;
        this.type = type;
        this.donations = donations;
        team = new ArrayList<ProjectGroups>();
    }

    public void addMember(ProjectGroups project) {
        team.add(project);
    }

    public void removeMember(ProjectGroups project) {
        team.remove(project);
    }

    public List<ProjectGroups> getMembers() {
        return team;
    }

    public String toString() {
        return "ProjectGroup : Name: " + name + ", Type: " + type + ", Donations: "
                + donations + " -> ";
    }
}
