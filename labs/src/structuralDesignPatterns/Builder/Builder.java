package structuralDesignPatterns.Builder;

public interface Builder {
    public void salary(String name, String salary);
}
