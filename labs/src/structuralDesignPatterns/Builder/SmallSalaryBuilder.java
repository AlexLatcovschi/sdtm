package structuralDesignPatterns.Builder;

public class SmallSalaryBuilder implements AdvancedBuilder {
    @Override
    public void smallSalary(String salary) {
        System.out.println(salary + " euro is a small salary for MD!");
    }

    @Override
    public void bigSalary(String salary) {
        //don't implement
    }
}
