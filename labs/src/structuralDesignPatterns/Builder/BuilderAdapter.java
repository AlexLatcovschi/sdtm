package structuralDesignPatterns.Builder;

public class BuilderAdapter implements Builder {
    AdvancedBuilder advancedBuilder;

    public BuilderAdapter(String name) {
        if (name.equalsIgnoreCase("Alex")) {
            advancedBuilder = new BigSalaryBuilder();
        } else if (name.equalsIgnoreCase("Mircea")) {
            advancedBuilder = new SmallSalaryBuilder();
        }
    }

    @Override
    public void salary(String name, String salary) {
        if (name.equalsIgnoreCase("Alex")) {
            advancedBuilder.bigSalary(salary);
        } else if (name.equalsIgnoreCase("Mircea")) {
            advancedBuilder.smallSalary(salary);
        }
    }
}
