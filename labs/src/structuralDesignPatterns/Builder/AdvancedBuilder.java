package structuralDesignPatterns.Builder;

public interface AdvancedBuilder {
    public void bigSalary(String salary);
    public void smallSalary(String salary);
}
