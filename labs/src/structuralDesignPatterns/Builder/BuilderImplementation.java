package structuralDesignPatterns.Builder;

public class BuilderImplementation implements Builder {
    BuilderAdapter builderAdapter;

    @Override
    public void salary(String name, String salary) {
        if (name.equalsIgnoreCase("Alex") || name.equalsIgnoreCase("Mircea")) {
            builderAdapter = new BuilderAdapter(name);
            builderAdapter.salary(name, salary);
        } else {
            System.out.println("Invalid name.");
        }
    }
}
