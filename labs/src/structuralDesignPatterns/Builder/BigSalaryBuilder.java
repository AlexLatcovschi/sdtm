package structuralDesignPatterns.Builder;

public class BigSalaryBuilder implements AdvancedBuilder {
    @Override
    public void bigSalary(String salary) {
        System.out.println(salary + " euro is a big salary for MD!");
    }

    @Override
    public void smallSalary(String salary) {
        //don't implement
    }
}
