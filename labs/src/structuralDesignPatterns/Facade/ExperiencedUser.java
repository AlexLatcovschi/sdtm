package structuralDesignPatterns.Facade;

public class ExperiencedUser {
    public static class Junior implements Employee {
        @Override
        public void feed() {
            System.out.println("A junior developer has been hired to work!");
        }
    }

    public static class Middle implements Employee {
        @Override
        public void feed() {
            System.out.println("A middle developer has been hired to work!");
        }
    }

    public static class Senior implements Employee {
        @Override
        public void feed() {
            System.out.println("A senior developer has been hired to work!");
        }
    }
}
