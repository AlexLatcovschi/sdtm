package structuralDesignPatterns.Facade;

public interface Employee {
    void feed();
}
