package structuralDesignPatterns.Facade;

public class EmployeeFeed {
    private Employee junior;
    private Employee middle;
    private Employee senior;

    public EmployeeFeed() {
        junior = new ExperiencedUser.Junior();
        middle = new ExperiencedUser.Middle();
        senior = new ExperiencedUser.Senior();
    }

    public void feedJunior() {
        junior.feed();
    }

    public void feedMiddle() {
        middle.feed();
    }

    public void feedSenior() {
        senior.feed();
    }
}
