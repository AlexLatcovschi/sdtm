package lab_1;

public final class Singleton {
    private static Singleton instance;
    public String firstName;
    public String lastName;
    public String phone;
    public String mail;
    public String gender;
    public Number age;

    private Singleton(String firstName, String lastName, String phone, String mail, String gender, Number age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.mail = mail;
        this.gender = gender;
        this.age = age;
    }

    public static Singleton getInstance(String firstName, String lastName, String phone, String mail, String gender, Number age) {
        if (instance == null) {
            instance = new Singleton(firstName, lastName, phone, mail, gender, age);
        }
        return instance;
    }
}