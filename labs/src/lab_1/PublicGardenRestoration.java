package lab_1;

public class PublicGardenRestoration extends ProjectName {
    @Override
    public int price() {
        return 35000;
    }

    @Override
    public String name() {
        return "Public Garden Restoration";
    }
}
