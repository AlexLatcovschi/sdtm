package lab_1;

public class ChildrenHomeRepair extends ProjectName {
    @Override
    public int price() {
        return 23000;
    }

    @Override
    public String name() {
        return "Children Home Repair";
    }
}
