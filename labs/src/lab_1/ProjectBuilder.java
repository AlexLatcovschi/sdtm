package lab_1;

public class ProjectBuilder {
    public ProjectType buildGardenProject() {
        ProjectType proj = new ProjectType();
        proj.addItem(new PublicGardenRestoration());
        return proj;
    }

    public ProjectType buildChildrenGardenProject() {
        ProjectType proj = new ProjectType();
        proj.addItem(new ChildrenHomeRepair());
        return proj;
    }
}
