package lab_1;

import java.util.ArrayList;
import java.util.List;

public class ProjectType {
    private List<ProjectName> items = new ArrayList<ProjectName>();

    public void addItem(ProjectName names) {
        items.add(names);
    }

    public void getCost() {
        for (ProjectName projects : items) {
            projects.price();
        }
    }

    public void showItems() {
        for (ProjectName projects : items) {
            System.out.print("Project name : " + projects.name() + "\n");
            System.out.println("Project price : " + projects.price());
        }
    }
}
