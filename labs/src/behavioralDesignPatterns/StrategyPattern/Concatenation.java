package behavioralDesignPatterns.StrategyPattern;

public class Concatenation implements Strategy {

    @Override
    public String concatenation(String a, String b) {
        return a + " " + b;
    }

}
