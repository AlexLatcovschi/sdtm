package behavioralDesignPatterns.StrategyPattern;

public interface Strategy {
    public String concatenation(String a, String b);
}
