package behavioralDesignPatterns.StrategyPattern;

public class Context {
    private Strategy strategy;

    public Context(Strategy strategy){
        this.strategy = strategy;
    }

    public String executeStrategy(String val1, String val2){
        return strategy.concatenation(val1, val2);
    }
}
