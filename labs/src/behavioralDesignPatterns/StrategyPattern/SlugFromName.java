package behavioralDesignPatterns.StrategyPattern;

public class SlugFromName implements Strategy {
    @Override
    public String concatenation(String a, String b) {
        String concatenated = (a + b);
        return Character.toLowerCase(concatenated.charAt(0)) + concatenated.substring(1);
    }
}
