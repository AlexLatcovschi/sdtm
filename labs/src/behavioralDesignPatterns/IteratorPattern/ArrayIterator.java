package behavioralDesignPatterns.IteratorPattern;

public class ArrayIterator implements Container {
    public String array[] = {};

    @Override
    public Iterator getIterator() {
        return new ArrayIteratorIterate();
    }

    private class ArrayIteratorIterate implements Iterator {
        int i;

        @Override
        public boolean hasNext() {
            if (i < array.length) {
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            if (this.hasNext()) {
                return array[i++];
            }
            return null;
        }
    }

    public void setArray(String[] array) {
        this.array = array;
    }
}
