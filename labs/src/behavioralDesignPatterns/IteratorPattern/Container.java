package behavioralDesignPatterns.IteratorPattern;

public interface Container {
    public Iterator getIterator();
}
