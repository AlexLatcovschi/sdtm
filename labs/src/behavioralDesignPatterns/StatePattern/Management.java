package behavioralDesignPatterns.StatePattern;

public class Management implements Connection {

    @Override
    public void open() {
        System.out.println("Open database for Management");
    }

    @Override
    public void close() {
        System.out.println("Close the database");
    }

    @Override
    public void log() {
        System.out.println("Log activities");
    }

    @Override
    public void update() {
        System.out.println("Management has been updated");
    }
}
