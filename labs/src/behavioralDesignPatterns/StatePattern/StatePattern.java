package behavioralDesignPatterns.StatePattern;

public class StatePattern {
    Controller controller;

    public StatePattern(String con) {
        controller = new Controller();
        // set trigger for management
        if (con.equalsIgnoreCase("management"))
            controller.setManagementConnection();
        controller.open();
        controller.log();
        controller.close();
        controller.update();
    }
}
