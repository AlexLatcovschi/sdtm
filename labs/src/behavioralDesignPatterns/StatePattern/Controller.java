package behavioralDesignPatterns.StatePattern;

public class Controller {
    public static Management management;

    private static Connection con;

    Controller() {
        management = new Management();
    }

    public void setManagementConnection() {
        con = management;
    }

    public void open() {
        con.open();
    }

    public void close() {
        con.close();
    }

    public void log() {
        con.log();
    }

    public void update() {
        con.update();
    }
}
