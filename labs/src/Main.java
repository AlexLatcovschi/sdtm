import behavioralDesignPatterns.IteratorPattern.ArrayIterator;
import behavioralDesignPatterns.IteratorPattern.Iterator;
import behavioralDesignPatterns.StatePattern.StatePattern;
import behavioralDesignPatterns.StrategyPattern.Concatenation;
import behavioralDesignPatterns.StrategyPattern.Context;
import behavioralDesignPatterns.StrategyPattern.SlugFromName;
import models.Employee;
import structuralDesignPatterns.Builder.BuilderImplementation;
import structuralDesignPatterns.Composite.ProjectGroups;
import structuralDesignPatterns.Facade.EmployeeFeed;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        // runLaboratory1();
        // runLaboratory2();
        runLaboratory3();
    }

    public static void runLaboratory1() {
        //////////////////////////////laboratory 1
        // lab_1.Singleton design pattern
        lab_1.Singleton singleton = lab_1.Singleton.getInstance("Alexandru", "Latcovschi", "063455627", "alexandru.latcovschi@gmail.com", "male", 20);
        lab_1.Singleton secondSingleton = lab_1.Singleton.getInstance("Alexandru", "Macedon", "", "", "male", 30);
        System.out.println("singleton: " + singleton.firstName + "\n" +
                singleton.lastName + "\n" + singleton.phone + "\n" + singleton.mail + "\n"
                + singleton.gender + "\n" + singleton.age + "\n");
        System.out.println("secondSingleton: " + secondSingleton.firstName + "\n" + secondSingleton.lastName + "\n"
                + secondSingleton.phone + "\n" + secondSingleton.mail + "\n" + secondSingleton.gender + "\n"
                + secondSingleton.age + "\n");

        // Builder design pattern
        lab_1.ProjectBuilder projectBuilder = new lab_1.ProjectBuilder();
        lab_1.ProjectType type1 = projectBuilder.buildChildrenGardenProject();
        type1.showItems();

        lab_1.ProjectType type2 = projectBuilder.buildGardenProject();
        type2.showItems();

        // models.Prototype design pattern
        // remove comments to introduce employee data from cmd
/*        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter Id: ");
        int id = Integer.parseInt(br.readLine());
        System.out.print("\n");

        System.out.print("Enter Name: ");
        String name = br.readLine();
        System.out.print("\n");

        System.out.print("Enter Address: ");
        String address = br.readLine();
        System.out.print("\n");

        System.out.print("Enter Salary: ");
        double salary = Double.parseDouble(br.readLine());
        System.out.print("\n");*/

//        models.Employee employee1 = new models.Employee(id, name, salary, address);
        System.out.println("\n");
        Employee employee1 = new Employee(1, "Alex", 30000, "Chisinau");

        employee1.show();
        System.out.println("\n");
        Employee employee2 = (Employee) employee1.getClone();
        employee2.show();
    }

    public static void runLaboratory2() {

        ////////////////////////////////////////////////////laboratory 2
        ////////////Builder DP
        BuilderImplementation builder = new BuilderImplementation();

        builder.salary("alex", "30000");
        builder.salary("Mircea", "200");
        builder.salary("Alex", "3000");
        builder.salary("Ionel", "4000");
        ////////////Composite DP
        System.out.println("\n\n");
        ProjectGroups p1 = new ProjectGroups("Unesco", 50000, "social");
        ProjectGroups p2 = new ProjectGroups("Duolingo", 30000, "application");
        ProjectGroups p3 = new ProjectGroups("AgroNet", 10000, "business");
        ProjectGroups p4 = new ProjectGroups("EStudy", 15000, "education");
        ProjectGroups p5 = new ProjectGroups("Pandemic", 20000, "social");


        p1.addMember(p2);
        p1.addMember(p3);
        p1.addMember(p4);

        p2.addMember(p5);

        p3.addMember(p1);
        p3.addMember(p5);

        for (ProjectGroups headGroup : p1.getMembers()) {
            System.out.println(headGroup);

            for (ProjectGroups employee : headGroup.getMembers()) {
                System.out.println(employee);
            }
        }

        ////////////Facade DP
        System.out.println("\n\n");
        EmployeeFeed employeeFeed = new EmployeeFeed();

        employeeFeed.feedJunior();
        employeeFeed.feedMiddle();
        employeeFeed.feedSenior();
    }

    public static void runLaboratory3() throws IOException {
        // Laboratory 3

        // Iterator Pattern
        String employeesNames[] = {"Alexandru Latcovschi",
                "Gradinaru Daniela", "Elon Musk", "Mihai Cebotari", "Nedelea Damian"};
        ArrayIterator arrayIterator = new ArrayIterator();
        arrayIterator.setArray(employeesNames);

        for (Iterator iter = arrayIterator.getIterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            System.out.println("FullName: " + name);
        }

        System.out.println("\n\n");

        // State Pattern
        new StatePattern("management");

        System.out.println("\n\n");

        // Strategy Pattern
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the first name: ");
        String value1 = br.readLine();

        System.out.print("Enter the last name: ");
        String value2 = br.readLine();

        System.out.println("\n\n");

        Context context = new Context(new Concatenation());
        System.out.println("Full Name = " + context.executeStrategy(value1, value2));

        Context context1 = new Context(new SlugFromName());
        System.out.println("Slug From Name = " + context1.executeStrategy(value1, value2));
    }
}
