package models;

public class Employee implements Prototype {

    private int id;
    private String name;
    private double salary;
    private String address;

    public Employee() {
        System.out.println("Id" + "\t" + "Name" + "\t" + "Salary" + "\t" + "Address");
    }

    public Employee(int id, String name, double salary, String address) {
        this();
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.address = address;
    }

    public void show() {
        System.out.println(id + "\t" + name + "\t" + salary + "\t" + address);
    }

    @Override
    public Prototype getClone() {
        return new Employee(id, name, salary, address);
    }
}