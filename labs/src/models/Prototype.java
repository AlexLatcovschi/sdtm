package models;

interface Prototype {
    public Prototype getClone();
}