STDM Laboratory work No.1
Topic: Creational Design Patterns
Prepared by : Latcovschi Alexandru , FAF-181
Evaluated by : Drumea Vasile

#Main tasks :
1. Choose an OO programming language and a suitable IDE or Editor (No frameworks/libs/engines allowed);

2. Select a domain area for the sample project;

3. Define the main involved classes and think about what instantiation mechanisms are needed;

4. Based on the previous point, implement atleast 3 creational design patterns in your project;

#Theory:
+ In software engineering, a design pattern is a general repeatable solution to a commonly occurring problem in software design. A design pattern isn't a finished design that can be transformed directly into code. It is a description or template for how to solve a problem that can be used in many different situations.
+ Design patterns can speed up the development process by providing tested, proven development paradigms. Effective software design requires considering issues that may not become visible until later in the implementation. Reusing design patterns helps to prevent subtle issues that can cause major problems and improves code readability for coders and architects familiar with the patterns.
+Design patterns: 
1. Abstract Factory - 
 Creates an instance of several families of classes
 2. Builder - 
 Separates object construction from its representation
 3. Factory Method - 
 Creates an instance of several derived classes
 4. Object Pool - 
 Avoid expensive acquisition and release of resources by recycling objects that are no longer in use
 5. models.Prototype - 
 A fully initialized instance to be copied or cloned
 6. lab_1.Singleton - 
 A class of which only a single instance can exist 
 
 #Implementation & Explanation
 
 In this project I implemented 3 design patterns (lab_1.Singleton, Builder, models.Prototype). All 3 are running in the Main class. I also have operating instances for all 3 patterns. In this project I chose as models some entities that could be used in a company that develops certain projects. So for lab_1.Singleton I created a special class that works with a simple user, and for example, in Main I created 2 users. For Builder pattern I created some projects that have name and price, respectively their values are set in the Main class and we can see the result in the output. For the prototype pattern I created a class for this, and further I implemented the cloning principle, with which I also work further in the Main class. Here we have 2 options for setting the value of the objects, we can from cmd, or hardcoded, for the first option we have to comment on a section of code.                                                              

 #Results:
 ![alt text](http://static.md/fb97a65d5330cfe6d0abdfe90329489f.png
)         

#Conclusion: 
In this lab I got acquainted with design patterns and found out how I can optimize the code using these examples. They help to write a code as clear and legible as possible, which is very good. So the purpose of this laboratory was achieved, and I understood better how to apply it theoretically in practice. Thank you very much!                                                