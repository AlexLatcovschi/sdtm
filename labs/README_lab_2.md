STDM Laboratory work No.2
Topic: Structural Design Patterns
Prepared by : Latcovschi Alexandru , FAF-181
Evaluated by : Drumea Vasile

#Main tasks :
1. Study and understand the Structural Design Patterns;

2. As a continuation of the previous laboratory work, think about the functionalities that your system will need to provide to the user;

3. Implement some additional functionalities using structural design patterns;

#Theory:
Structural design patterns are concerned with how classes and objects can be composed, to form larger structures.

The structural design patterns simplifies the structure by identifying the relationships.

These patterns focus on, how the classes inherit from each other and how they are composed from other classes.

##Types of structural design patterns
1.Adapter Pattern
Adapting an interface into another according to client expectation.

2.Bridge Pattern
Separating abstraction (interface) from implementation.

3.Composite Pattern
Allowing clients to operate on hierarchy of objects.

4.Decorator Pattern
Adding functionality to an object dynamically.

5.Facade Pattern
Providing an interface to a set of interfaces.

6.Flyweight Pattern
Reusing an object by sharing it.

7.proxy Pattern
Representing another object.
 
 #Implementation & Explanation
 
 In this project I implemented 3 Structural Design Patterns:
  (Builder, Composite, Facade).
   All 3 are running in the Main class.
    I also have operating instances for all 3 patterns.
     In this project I chose as models some entities that could be used in a company that develops
      certain projects. So for Builder I created a special class that works with a simple
       user and shows his salary. For Composite pattern I show some project group description and
       team members. For the Facade pattern I just feed 3 types of users: junior, middle, senior -> 
       developers in company :)                                                             

## To run laboratory 2 just run runLaboratory2() from main!   

 #Results:
30000 euro is a big salary for MD!
200 euro is a small salary for MD!
3000 euro is a big salary for MD!
Invalid name.



ProjectGroup : Name: Duolingo, Type: application, Donations: 30000 -> 
ProjectGroup : Name: Pandemic, Type: social, Donations: 20000 -> 
ProjectGroup : Name: AgroNet, Type: business, Donations: 10000 -> 
ProjectGroup : Name: Unesco, Type: social, Donations: 50000 -> 
ProjectGroup : Name: Pandemic, Type: social, Donations: 20000 -> 
ProjectGroup : Name: EStudy, Type: education, Donations: 15000 -> 



A junior developer has been hired to work!
A middle developer has been hired to work!
A senior developer has been hired to work!         

#Conclusion: 
In this lab we learned how to combine classes to create larger structures that make our work much easier. I learned that there are 7 types of structural design patterns. Even if in this laboratory I have not implemented all of them, I have studied them and I know that they will fit well in the future. I already work in an IT company and I apply them in my day job.